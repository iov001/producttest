package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

func httpGet(inter Handler_inter) (string, error) {
	urlall := fmt.Sprintf("%s?command=%s", inter.ServerUrl(), inter.Command())
	client := &http.Client{
		Timeout: time.Duration(inter.Timeout()) * time.Second,
	}
	resp, err := client.Get(urlall)
	if err != nil {
		// handle error
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		// handle error
		return "", err
	}
	fmt.Println("response:", string(body))
	return string(body), nil
}
