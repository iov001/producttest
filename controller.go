package main

import (
	//"errors"
	"fmt"
	"github.com/lxn/walk"
	//. "github.com/lxn/walk/declarative"
	//"log"
	"reflect"
	"strings"
	"time"
)

type Handler_inter interface {
	Handler(string) (string, error)
	ServerUrl() string
	Timeout() int32
	Command() string
	Alias() string
}

func main_test() {
	go loop_test()
}

func test_temperature() {
	for i := 0; i < 100; i++ {
		outTE4.SetText(fmt.Sprintf("%d", i))
		time.Sleep(1 * time.Second)
	}
	return
}
func loop_test() bool {

	//fmt.Println(ManuAffirmDialog())

	logDesk.AppendText("begin test!\n")
	isSpecialMode.SetSatisfied(!isSpecialMode.Satisfied())
	defer isSpecialMode.SetSatisfied(!isSpecialMode.Satisfied())

	chanHttp := make(chan int)
	go loop_by_http(chanHttp)

	<-chanHttp

	return true
}
func append_out(te *walk.TextEdit, args ...string) {
	for _, arg := range args {
		te.AppendText(arg)
	}
}

func loop_by_http(ch chan int) {
	elems := reflect.ValueOf(&GItemMark).Elem()

	for i := 0; i < elems.NumField(); i++ {

		elem := elems.Field(i)
		typeofElems := elems.Type()
		k := typeofElems.Field(i).Name
		vi := elem.Interface()

		switch vi.(type) {
		case bool:
			if vi.(bool) == true {
				v := Muxm[strings.ToLower(k)]
				append_out(logDesk, "Testing: ", k, "...\n")
				//connect http server and send test command
				rsp, err := httpGet(v)
				if err != nil {
					append_out(logDesk, err.Error(), "\n", "Connect failed\n")
					ch <- 1
					return
				}
				//deal response use handler
				detail, err := v.Handler(rsp)
				if err != nil {
					append_out(logDesk, err.Error(), "\n", detail, "\n")
					append_out(progressDesk, "测试", v.Alias(), "错误\n")
					append_out(faultDesk, "测试", v.Alias(), "错误\n")
					continue
				}
				append_out(logDesk, detail, "\n", "Test OK!\n\n")
				append_out(progressDesk, "测试", v.Alias(), "通过\n")
			}
		default:
			continue
		}
	}
	ch <- 1
	return
}
