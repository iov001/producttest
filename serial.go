package main

import (
	"fmt"
	//"log"
	"errors"
	"github.com/tarm/serial"
	"strings"
	"time"
)

type login struct {
	loginSign  string
	passwdSign string
	inputSign  string
	user       string
	password   string
}

func test_main() {
	s, _ := serial_init()

	err, ok := (&login{
		loginSign:  "login:",
		passwdSign: "Password:",
		inputSign:  "-sh-4.4#",
		user:       "root",
		password:   "Fayxzx00",
	}).loginfunc(s)
	if ok {
		fmt.Println("login success!")
	} else {
		if err != nil {
			fmt.Println(err)
		}
	}
	/*
		n, err := write_serial(s, "ifconfig")
		if err != nil {
			fmt.Println("write fail! get", n)
			return
		}
	*/

}
func serial_init() (*serial.Port, error) {
	c := &serial.Config{Name: "COM3", Baud: 115200, ReadTimeout: 5 * time.Second}
	s, err := serial.OpenPort(c)
	if err != nil {
		return s, err
	}
	return s, nil
}
func (this *login) loginfunc(s *serial.Port) (error, bool) {
	readbuf, err := read_serial(s)
	if err != nil {
		return err, false
	}
	//fmt.Println("read buf=", readbuf)
	if !strings.Contains(readbuf, this.inputSign) {
		if strings.Contains(readbuf, this.inputSign) {
			//fmt.Println("login success!!")
			return nil, true
		}
		//fmt.Println("didnt get login: first")
		write_serial(s, "\n")
		readbuf, _ := read_serial(s)
		if !strings.Contains(readbuf, this.loginSign) {
			fmt.Println("didnt get login: second")
			return errors.New("didn't get login sign!"), false
		}
	}
	fmt.Println("get login:")

	_, err = write_serial(s, this.user)
	readbuf, err = read_serial(s)
	if strings.Contains(readbuf, this.passwdSign) {
		_, err = write_serial(s, this.password)
	}
	readbuf, err = read_serial(s)
	if !strings.Contains(readbuf, this.loginSign) {
		return errors.New("password failed"), false
	}
	//fmt.Println("login success!!")
	return nil, true
}

func write_serial(s *serial.Port, content string) (int, error) {

	n, err := s.Write([]byte(fmt.Sprintf("%s\n", content)))
	if err != nil {
		fmt.Println(err, n)
		return n, err
	}
	s.Flush()
	return n, nil
}
func read_serial(s *serial.Port) (string, error) {
	num := 0
	respSlice := make([]string, 10)
	for {
		time.Sleep(1 * time.Second)
		buf := make([]byte, 1024)
		n, err := s.Read(buf)
		if err != nil {
			return "", err
		}
		if n == 0 {
			fmt.Println("serial read finished")
			break
		}
		tmp := buf[:n]
		respSlice = append(respSlice, string(tmp))
		num++
	}

	fmt.Println("serial read", num, "times altogether.")
	return strings.Join(respSlice, ""), nil
}
