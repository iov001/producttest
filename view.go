package main

import (
	"fmt"
	"github.com/lxn/walk"
	. "github.com/lxn/walk/declarative"
	"log"
)

type MyMainWindow struct {
	*walk.MainWindow
}

var progressDesk *walk.TextEdit
var faultDesk *walk.TextEdit
var logDesk *walk.TextEdit
var outTE4 *walk.TextEdit
var isSpecialMode = walk.NewMutableCondition()

func view() {
	MustRegisterCondition("isSpecialMode", isSpecialMode)

	isSpecialMode.SetSatisfied(!isSpecialMode.Satisfied())
	if err := (MainWindow{
		AssignTo: &MW.MainWindow,
		Title:    "测试",
		MinSize:  Size{600, 400},
		Layout:   VBox{},
		Children: []Widget{
			VSplitter{
				Children: []Widget{
					PushButton{
						Text:    "开始测试",
						Enabled: Bind("isSpecialMode"),
						OnClicked: func() {
							main_test()
						},
					},
					HSplitter{
						Children: []Widget{
							PushButton{
								Text:      "停止",
								OnClicked: func() {},
							},
							PushButton{
								Text: "设置",
								OnClicked: func() {
									if cmd, err := RunSetDialog(); err != nil {
										log.Print(err)
									} else if cmd == walk.DlgCmdOK {
										fmt.Printf("%+v\n", GItemMark)
									}
								},
							},
						},
					},
				},
			},

			HSplitter{
				Children: []Widget{
					Label{Text: "进度"},
					Label{Text: "错误"},
				},
			},
			HSplitter{
				Children: []Widget{
					TextEdit{AssignTo: &progressDesk, ReadOnly: true},
					TextEdit{AssignTo: &faultDesk, ReadOnly: true},
				},
			},
			Label{Text: "详情"},
			TextEdit{AssignTo: &logDesk, ReadOnly: true},
		},
	}.Create()); err != nil {
		log.Fatal(err)
	}

	return
}
func ManuAffirmDialog() string {
	var result string
	var dlg *walk.Dialog
	var acceptPB, cancelPB *walk.PushButton

	Dialog{
		AssignTo:      &dlg,
		Title:         "确认",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,

		MinSize: Size{300, 300},
		Layout:  VBox{},
		Children: []Widget{
			Label{
				Text: "预测结果：lte 1 / sim 1 / Ge 2 3 灯亮。",
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "正确 ",
						OnClicked: func() {
							result = "right"
							dlg.Cancel()
						},
					},
					PushButton{
						AssignTo: &cancelPB,
						Text:     "错误",
						OnClicked: func() {
							result = "fault"
							dlg.Cancel()
						},
					},
				},
			},
		},
	}.Run(MW)

	return result
}
func RunSetDialog() (int, error) {
	var dlg *walk.Dialog
	var db *walk.DataBinder
	var ep walk.ErrorPresenter
	var acceptPB, cancelPB *walk.PushButton
	return Dialog{
		AssignTo:      &dlg,
		Title:         "设置",
		DefaultButton: &acceptPB,
		CancelButton:  &cancelPB,
		DataBinder: DataBinder{
			AssignTo:       &db,
			DataSource:     &GItemMark,
			ErrorPresenter: ErrorPresenterRef{&ep},
		},
		MinSize: Size{300, 300},
		Layout:  VBox{},
		Children: []Widget{
			Composite{
				Layout: Grid{Columns: 2},
				Children: []Widget{
					Label{
						Text: "硬盘",
					},
					CheckBox{
						Checked: Bind("Hd"),
					},
					Label{
						Text: "Standard",
					},
					CheckBox{
						Checked: Bind("Standard"),
					},
				},
			},
			Composite{
				Layout: HBox{},
				Children: []Widget{
					HSpacer{},
					PushButton{
						AssignTo: &acceptPB,
						Text:     "确认",
						OnClicked: func() {
							if err := db.Submit(); err != nil {
								log.Print(err)
								return
							}
							dlg.Accept()
						},
					},
					PushButton{
						AssignTo:  &cancelPB,
						Text:      "取消",
						OnClicked: func() { dlg.Cancel() },
					},
				},
			},
		},
	}.Run(MW)
}
