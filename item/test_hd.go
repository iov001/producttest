package item

import (
	"encoding/json"
	"errors"
)

type HdTest struct {
	//{"input":2342,"output":324342,"tx":22,"rx":43}
	Input  int32
	Output int32
	Tx     int32
	Rx     int32
}

var HdTestVar = &HdTest{}

func (this *HdTest) Handler(response string) (string, error) {
	/*
	 * 解析json字符串
	 */
	err := json.Unmarshal([]byte(response), this)
	if err != nil {
		return response, err
	}

	/*
	 * 判断解析到的参数是否符合标准.
	 */
	if this.Input == 2342 {
		return response, nil
	}
	return response, errors.New("Test failed!")
}

func (this *HdTest) ServerUrl() string {
	return "http://192.168.0.1:8000/io.php"
}
func (this *HdTest) Timeout() int32 {
	return 20
}
func (this *HdTest) Command() string {
	return "/usr/sbin/test_hd.sh"
}
func (this *HdTest) Alias() string {
	return "硬盘"
}
