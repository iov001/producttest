package item

import (
	"encoding/json"
	"errors"
)

type StandardTest struct {
	//{"cpu":"30%","mem":"27%","tempture":"40"}
	Cpu      string
	Mem      string
	Tempture int32
}

var StandardTestVar = &StandardTest{}

func (this *StandardTest) Handler(response string) (string, error) {
	/*
	 * 解析json字符串
	 */
	err := json.Unmarshal([]byte(response), this)
	if err != nil {
		return response, err
	}

	/*
	 * 判断解析到的参数是否符合标准.
	 */
	if this.Tempture == 40 {
		return response, nil
	}
	return response, errors.New("Test failed!")
}

func (this *StandardTest) ServerUrl() string {
	return "http://192.168.0.1:8000/standard.php"
}
func (this *StandardTest) Timeout() int32 {
	return 5
}
func (this *StandardTest) Command() string {
	return "/usr/sbin/test_standard.sh"
}
func (this *StandardTest) Alias() string {
	return "标准"
}
