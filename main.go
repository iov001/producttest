package main

import (
	//"github.com/lxn/walk"
	"gui/item"
	//. "github.com/lxn/walk/declarative"
)

type TestItemMark struct {
	Hd       bool
	Standard bool
}

var GItemMark = TestItemMark{true, true}
var Muxm map[string]Handler_inter
var MW MyMainWindow

func init() {
	Muxm = make(map[string]Handler_inter)
	route()
	view()
}

func route() {
	Muxm["hd"] = item.HdTestVar
	Muxm["standard"] = item.StandardTestVar
}

func main() {
	MW.Run()
	return
}
